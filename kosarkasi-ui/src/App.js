import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import axios from './axios-orders';
import Header from './components/Header';
import RegistrationForm from './components/RegistrationForm';
import Table from './components/Table';
import EditForm from './components/EditForm';
import SearchForm from './components/SearchForm';
import './Site.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            authorized: false,
            registrationActive: false,
            registrationSucceded: false,
            email: '',
            password: '',
            registrationMessage: '',
            token: '',
            actionName: 'registration',
            kosarkasi: [],
            kosarkas: {},
            edit: false,
            klubovi: []

        };

        this.onSubmitRegistrationHandler = this.onSubmitRegistrationHandler.bind(this);

    }

    componentDidMount() {
        //console.log('COMPONENT DID MOUNT');
        this.getKosarkasi();
    }

    getKosarkasi() {
        axios.get('api/kosarkasi')
            .then(response => {
                //console.log("GET KOSARKASI");
                this.setState({ kosarkasi: response.data });
                //console.log(response.data);
            })
            .catch(error => {
                alert('Greska prilikom dobavljanja kosarkasa sa servera!')
                //this.setState({ error: true });
            });
    }


    labelTextHandler = () => {
        if (!this.state.authorized && !this.state.registrationActive) {
            return "Korisnik nije prijavljen na sistem!";
        }
        else if (!this.state.authorized && this.state.registrationActive) {
            return "Registracija i prijava korisnika";
        }
        else if (this.state.authorized && !this.state.registrationActive) {
            return "Prijavljeni korisnik:" + this.state.email;
        }

    }

    buttonActionHandler = () => {

        switch (this.state.actionName) {
            case ('registration'):
                this.setState({ registrationActive: true, actionName: 'home' });
                this.labelTextHandler();
                break;
            case ('home'):
                this.setState({ registrationActive: false, actionName: 'registration' });
                this.labelTextHandler();
                break;
            case ('logOut'):
                this.setState({ authorized: false, actionName: 'registration', edit: false });
                this.getKosarkasi();
                this.labelTextHandler();
                break;
            default:

        }


    }

    onSubmitRegistrationHandler = (event) => {
        event.preventDefault();
        const updatedEmail = event.target.elements.email.value;
        const updatedPassword = event.target.elements.password.value;
        //console.log("on submit");
        //console.log(this.state);

        const sendData = {
            Email: updatedEmail,
            Password: updatedPassword,
            ConfirmPassword: updatedPassword
        };

        console.log(sendData);

        axios.post('api/Account/Register', sendData)
            .then(response => {
                this.setState({
                    ...this.state,
                    registrationMessage: 'Uspesna registracija na sistem!',
                    registrationSucceded: true

                }, () => { document.getElementById("registrationForm").reset(); });
                console.log("uspesno");

            })
            .catch(error => {
                console.log(error);
                alert('Greska prilikom registracije!');
            });
    }




    onSubmitLogInHandler = (event) => {
        event.preventDefault();

        const updatedEmail = document.getElementById("registrationForm").elements.email.value;
        const updatedPassword = document.getElementById("registrationForm").elements.password.value;

        const sendData = {
            "grant_type": "password",
            "username": updatedEmail,
            "password": updatedPassword
        };

        console.log(sendData);
        var qs = require('qs');
        axios.post('Token', qs.stringify(sendData))
            .then(response => {
                this.setState({
                    ...this.state,
                    registrationMessage: '',
                    registrationActive: false,
                    email: response.data.userName,
                    token: response.data.access_token,
                    authorized: true,
                    actionName: 'logOut'

                });
                //console.log(response);
            })
            .catch(error => {
                //console.log(error);
                alert('Greska prilikom prijave!');
            });

    }

    itemDeleteHandler = (id) => {
        //console.log("delete");
        //console.log(id);

        let headers = {};

        if (this.state.authorized) {

            console.log(this.state.token);
            console.log(headers);

            let authorizationToken = 'Bearer ' + this.state.token;
            axios.delete('api/kosarkasi/' + id, {
                headers: {
                    "Authorization": authorizationToken
                }
            })
                .then(response => {
                    this.getKosarkasi();
                })
                .catch(error => {
                    alert('Greska rpilikom brisanja!');
                });

        }
        else {
            alert('unauthorized request');
        }

    }

    getKlubovi = () => {
        axios.get('api/klubovi')
            .then(response => {
                this.setState({ klubovi: response.data });
                //console.log("KLUBOVI!!!!");
                //console.log(response.data);
            })
            .catch(error => {
                alert('Greska prilikom preuzimanja klubova!');
                //this.setState({ error: true });
            });
    }

    itemEditHandler = (id) => {
        let headers = {};

        if (this.state.authorized) {

            this.getKlubovi();

            //console.log(this.state.token);
            //console.log(headers);

            let authorizationToken = 'Bearer ' + this.state.token;
            axios.get('api/kosarkasi/' + id, {
                headers: {
                    "Authorization": authorizationToken
                }
            })
                .then(response => {
                    //console.log('uspesan get za edit');
                    //console.log(response);
                    this.setState({
                        ...this.state,
                        kosarkas: response.data,
                        edit: true
                    });
                })
                .catch(error => {
                    alert('Greska prilikom preuzimanja kosarkasa!');
                    //console.log(error);
                });

        }
        else {
            alert('unauthorized request');
        }

    }

    editSubmitHandler = (sendData) => {

        let headers = {};

        //console.log("app edit data");
        //console.log(sendData);
        //console.log(sendData.Id);

        if (this.state.authorized) {

            let authorizationToken = 'Bearer ' + this.state.token;

            axios.put('api/kosarkasi/' + sendData.Id, sendData,
                {
                    headers: {
                        "Authorization": authorizationToken
                    }
                })
                .then(response => {
                    this.setState({
                        ...this.state,
                        edit: false,
                    });
                    this.getKosarkasi();

                })
                .catch(error => {
                    alert('Greska prilikom izmene kosarkasa!');
                    //console.log(error);
                });

        }
        else {
            alert('Unauthorized request!');
        }

    }

    cancelHandler = (event) => {
        event.preventDefault();
        //console.log("CANCEL");
        this.setState({
            ...this.state,
            edit: false
        });
        //document.getElementById("editForm").reset();

    }

    searchHandler = (event) => {
        event.preventDefault();

        let najmanje = Number(event.target.elements.najmanje.value);
        let najvise = Number(event.target.elements.najvise.value);


        if (!najmanje || !najvise) {
            this.getKosarkasi();
            return;
        }

        if (najmanje > najvise) {
            alert("Najmanje mora biti manje od najvise");           
            return;
        }

        const sendData = {
            "Najmanje": najmanje,
            "Najvise": najvise
        }

        if (this.state.authorized) {

            let authorizationToken = 'Bearer ' + this.state.token;

            axios.post('api/pretraga', sendData,
                {
                    headers: {
                        "Authorization": authorizationToken
                    }
                })
                .then(response => {
                    this.setState({
                        ...this.state,
                        kosarkasi: response.data
                    }, () => { document.getElementById("searchForm").reset(); });


                })
                .catch(error => {
                    alert('Greska prilikom pretrage!');
                    //console.log(error);
                });

        }
        else {
            alert('Unauthorized request!');
        }

    }






    render() {

        let registrationForm = '';

        if (this.state.registrationActive) {
            registrationForm =
                <RegistrationForm
                success={this.state.registrationSucceded}
                submit={this.onSubmitRegistrationHandler}
                clicked={this.onSubmitLogInHandler}
                email={this.state.email}
                password={this.state.password}
                />
        }

        let editForm = <div></div>;

        if (this.state.authorized && this.state.edit) {
            editForm = <EditForm
                kosarkas={this.state.kosarkas}
                klubovi={this.state.klubovi}
                submit={this.editSubmitHandler}
                clicked={this.cancelHandler}
            />
        }

        let searchForm = '';

        if (this.state.authorized) {
            searchForm =
                <SearchForm
                submit={this.searchHandler}
                />
        }


        return (
            <div className={"container"}>
                <Header
                    label={this.labelTextHandler()}
                    actionName={this.state.actionName}
                    clicked={this.buttonActionHandler}
                />
                {registrationForm}
                <div style={{ paddingTop: '30px', textAlign: 'center', color: 'lightblue'}}>{this.state.registrationMessage}</div>
                <div style={{ paddingTop: '30px', textAlign: 'center', paddingBottom: '20px' }}><h2>Kosarkasi</h2></div>
                <div className={"container"}>
                    <div className={"row"}>
                        <Table
                            authorized={this.state.authorized}
                            kosarkasi={this.state.kosarkasi}
                            btnDelete={this.itemDeleteHandler}
                            btnEdit={this.itemEditHandler}
                        />
                        {searchForm}
                    </div>
                </div>
                {editForm}
            </div>
        );
    }

}

export default App;


