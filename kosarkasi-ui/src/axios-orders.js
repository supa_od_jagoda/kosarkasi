import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:50819/'
});

export default instance;