import React from 'react';


const input = (props) => {

    let inputElement = null;

    let classes = "form-control col-sm-4";

    if (props.invalid && props.shouldValidate && props.touched) {
        classes = "form-control col-sm-4 is-invalid";
    }

    console.log(props);

    switch (props.elementType) {
        case ('input'):
            inputElement = <input
                className={classes}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case ('select'):
            inputElement = (
                <select
                    className={classes}
                    onChange={props.changed}>
                    {props.klubovi.map(klub => (
                        <option key={klub.Id} value={klub.Id} selected={props.value === klub.Id}>
                            {klub.Naziv}
                        </option>
                    ))}
                </select>
            );
            break;
        default:
            inputElement = <input
                {...props.elementConfig}
                onChange={props.changed} />;
    }

    return (
        <div className={"form-group row"} style={{ justifyContent: 'center' }}>
            <label className={"col-sm-3 col-form-label"}>{props.elementConfig.label}</label>
            {inputElement}
            <div className={"row invalid-feedback"} style={{ textAlign: 'right' }}>{props.errorMessage}</div>
        </div>
    );
};

export default input;