import React from 'react';
import '../Site.css';



const button = (props) => {

    let classes = ["button"];

    if (props.color) {
        classes = ["button yellow"];
    }

    return (
        <button
            className = {classes}
            onClick = { props.clicked }
        > { props.children }</button >

)};

export default button;

