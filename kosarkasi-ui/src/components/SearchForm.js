import React from 'react';
import Input from './Input';
import Button from './Button';
import axios from '../axios-orders';

const searchForm = props => {

    //console.log('SEARCHFORM');

    let classesRow = "form-group row";
    let classesLabel = "col-sm-3 col-form-label";
    let classesInput = "form-control col-sm-4";

    let style = { justifyContent: 'center' };


    return (
        <div className={"col"}>
            <div style={{ paddingTop: '50px', textAlign: 'center', paddingBottom: '20px' }}><h4>Pretraga po broju utakmica</h4></div>
            <form onSubmit={props.submit} id="searchForm">
                <div className={classesRow} style={style}>
                    <label className={classesLabel}>Najmanje</label>
                    <input 
                        className={classesInput}
                        name="najmanje"
                        type="number"
                        />
                </div>
                <div className={classesRow} style={style}>
                    <label className={classesLabel}>Najvise</label>
                    <input
                        className={classesInput}
                        name="najvise"
                        type="number"
                        />
                </div>
                <div className={classesRow} style={{ justifyContent: 'center', paddingTop: '30px' }}>
                    <Button>Pretraga</Button>
                </div>
            </form>
        </div>
    );

};


export default searchForm;


