import React, { } from 'react';
import TableRow from './TableRow';

const table = (props) => {

    let tableHeader;
    if (!props.authorized) {
        tableHeader = <tr style={{ backgroundColor: 'lemonchiffon' }}><th>Ime i Prezime</th><th>Rodjenje</th><th>Klub</th><th>Utakmice</th><th>Poeni</th></tr>;
    }

    else {
        tableHeader = <tr style={{ backgroundColor: 'lemonchiffon' }}><th>Ime i Prezime</th><th>Rodjenje</th><th>Klub</th><th>Utakmice</th><th>Poeni</th><th>Brisanje</th><th>Izmena</th></tr>;
    }

    //console.log("table kosarkasi");
    //console.log(props.kosarkasi);

    return (
        <div className={"col"}>
            <table className={"table table-bordered table-hover"}>
                <tbody>
                    {tableHeader}
                    {props.kosarkasi.map(kosarkas => (
                        <TableRow
                            key={kosarkas.Id}
                            id={kosarkas.Id}
                            name={kosarkas.ImePrezime}
                            year={kosarkas.GodinaRodjenja}
                            club={kosarkas.KlubNaziv}
                            games={kosarkas.BrUtakmica}
                            points={kosarkas.PoeniProsek}
                            authorized={props.authorized}
                            btnDelete={props.btnDelete}
                            btnEdit={props.btnEdit}
                        />
                    ))}

                </tbody>
            </table>
        </div>
    );

};

export default React.memo(table);

