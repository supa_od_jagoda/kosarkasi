import React, { useState } from 'react';
import Input from './Input';
import Button from './Button';
import axios from '../axios-orders';

const registrationForm = props => {

    let classesRow = "form-group row";
    let classesLabel = "col-sm-3 col-form-label";
    let classesInput = "form-control col-sm-4";

    let style = { justifyContent: 'center' };

    return (
        <div> 
            <form onSubmit={props.submit} id="registrationForm">
                <div className={classesRow} style={style}>
                    <label className={classesLabel}>Korisnicko ime - email:</label>
                    <input className={classesInput}
                        name="email"
                        type="email"
                        />
                </div>
                <div className={classesRow} style={style}>
                    <label className={classesLabel}>Lozinka:</label>
                    <input className={classesInput}
                        name="password"
                        type="password"
                        />
                </div>
                <div className={classesRow} style={{ justifyContent: 'center', paddingTop: '30px' }}>
                    <div className={"col-sm-3"}><Button color>Registracija</Button></div>
                    <div className={"col-sm-3"}><Button clicked={props.clicked}>Prijava</Button></div>
                </div>
            </form>
        </div>
    );

};


export default registrationForm;


