import React, { useState } from 'react';
import Input from './Input';
import Button from './Button';
import axios from '../axios-orders';

const editForm = props => {

    const initState = {
        name: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                label: 'Ime i prezime: ',
            },
            value: '',
            validation: {
                required: true,
                minLength: 1,
                maxLength: 40
            },
            status: {
                valid: false,
                errorMessage: ''
            },

            touched: false

        },
        year: {
            elementType: 'input',
            elementConfig: {
                type: 'number',
                label: 'Godina rodjenja: ',
            },
            value: '',
            validation: {
                required: true,
                minValue: 1975,
                maxValue: 2000
            },
            status: {
                valid: false,
                errorMessage: ''
            },
            touched: false
        },

        club: {
            elementType: 'select',
            elementConfig: {
                label: 'Broj utakmica: ',
            },
            value: '',
            //validation: {},
            status: {
                valid: true,
                errorMessage: ''
            }
        },

        games: {
            elementType: 'input',
            elementConfig: {
                type: 'number',
                label: 'Broj utakmica: ',
            },
            value: '',
            validation: {
                required: true,
                minValue: 1
            },
            status: {
                valid: false,
                errorMessage: ''
            },
            touched: false
        },

        points: {
            elementType: 'input',
            elementConfig: {
                type: 'number',
                label: 'Prosecno poena: ',
            },
            value: '',
            validation: {
                required: true,
                minValue: 0,
                maxValue: 30
            },
            status: {
                valid: false,
                errorMessage: ''
            },
            touched: false
        },

    };

    initState.club.value = props.kosarkas.KlubId;
    initState.games.value = props.kosarkas.BrUtakmica;
    initState.name.value = props.kosarkas.ImePrezime;
    initState.points.value = props.kosarkas.PoeniProsek;
    initState.year.value = props.kosarkas.GodinaRodjenja;    

    const [editForm, setEditForm] = useState(initState);

    React.useEffect(() => {
        setEditForm(initState);
    }, [props.kosarkas.Id])

    const formSubmitHandler = (event) => {

        event.preventDefault();

        const formData = {};

        for (let formElementIdentifier in editForm) {
            formData[formElementIdentifier] = editForm[formElementIdentifier].value;
        }


        const sendData = {
            "Id": props.kosarkas.Id,
            "ImePrezime": formData.name,
            "GodinaRodjenja": formData.year,
            "BrUtakmica": formData.games,
            "PoeniProsek": formData.points,
            "KlubId": formData.club,
        }


        props.submit(sendData);

    }

    const checkValidity = (value, rules) => {
        let status = {
            "isValid": true,
            "errorMessage": ""
        };
        if (!rules) {
            return status;
        }

        if (rules.required) {
            if (value.trim() === '') {
                status.isValid = false;
                status.errorMessage = "Ovo polje je obavezno";
                return status;
            }
        }

        if (rules.minLength) {
            if (value.length < rules.minLength) {
                status.isValid = false;
                status.errorMessage = "Tekst mora imati minimalno " + rules.minLength + " karakter";
                return status;
            }
        }

        if (rules.maxLength) {
            if (value.length > rules.maxLength) {
                status.isValid = false;
                status.errorMessage = "Tekst moze imati maksimalno " + rules.maxLength + " karaktera";
                return status;
            }
        }

        if (rules.minValue) {
            if (value < rules.minValue) {
                status.isValid = false;
                status.errorMessage = "Minimalna vrednost je " + rules.minValue;
                return status;
            }
        }

        if (rules.maxValue) {
            if (value > rules.maxValue) {
                status.isValid = false;
                status.errorMessage = "Maksimalna vrednost je " + rules.maxValue;
                return status;
            }
                
        }

        return status;
    }

    const inputChangedHandler = (event, inputIdentifier) => {

        const updatedEditForm = {
            ...editForm
        };

        const updatedFormElement = {
            ...updatedEditForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;
        updatedFormElement.touched = true;

        let status = checkValidity(
            event.target.value,
            editForm[inputIdentifier].validation
        );


        updatedFormElement.status.valid = status.isValid;
        updatedFormElement.status.errorMessage = status.errorMessage;


        updatedEditForm[inputIdentifier] = updatedFormElement;
        setEditForm(updatedEditForm);
    };


    const formElementsArray = [];
    for (let key in editForm) {
        formElementsArray.push({
            id: key,
            config: editForm[key]
        });
    }

    //console.log('EditForm Kosarkasi');
    //console.log(props.kosarkas);

    return (
        <div>
            <form onSubmit={formSubmitHandler} id="editForm">
                <div style={{ paddingTop: '30px', textAlign: 'center', paddingBottom: '20px' }}><h2>Izmena kosarkasa</h2></div>
                {formElementsArray.map(formElement => (
                    <Input
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        klubovi={props.klubovi}
                        invalid={!formElement.config.status.valid}
                        errorMessage={formElement.config.status.errorMessage}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={event => inputChangedHandler(event, formElement.id)}
                    />
                ))};
                <div className={"form-group row"} style={{ justifyContent: 'center', paddingTop: '30px' }}>
                    <div className={"col-sm-3"}><Button color>Izmena</Button></div>
                    <div className={"col-sm-3"}><Button clicked={props.clicked}>Odustajanje</Button></div>
                </div>
            </form>
        </div>
    );

};


export default editForm;
