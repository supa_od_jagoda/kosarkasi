import React from 'react';
import Button from './Button';

const header = props => {

    let btnText = '';

    switch (props.actionName) {
        case ('registration'):
            btnText = 'Registracija i Prijava';
            break;
        case ('home'):
            btnText = 'Pocetak';
            break;
        case ('logOut'):
            btnText = 'Odjava';
            break;
        default:
            btnText = '';
    };

    return (
        <div style={{ marginTop: '50px', paddingBottom: '50px' }} className={'row'} >
            <div className={'col-auto mr-auto'}><label>{props.label}</label></div>
            <div className={'col-auto'}><Button clicked={props.clicked} color>{btnText}</Button></div>
        </div>
    );
};
    
export default header;