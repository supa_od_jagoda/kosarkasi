import React from 'react';


const tableRow = (props) => {
    //console.log("tablerow");
    //console.log(props);

    function deleteHandler(event) {
        props.btnDelete(event.target.name);
    }

    function editHandler(event) {
        props.btnEdit(event.target.name);
    }

    let tableRow;
    let buttonDelete = <td><button onClick={deleteHandler} name={props.id} className={'btn btn-default btn-md'}>[Obrisi]</button></td>;
    let buttonEdit = <td><button onClick={editHandler} name={props.id} className={'btn btn-default btn-md'}>[Izmeni]</button></td>;

    if (!props.authorized) {
        tableRow = <tr><td>{props.name}</td><td>{props.year}</td><td>{props.club}</td><td>{props.games}</td><td>{props.points}</td></tr>;
    }

    else {
        tableRow = <tr><td>{props.name}</td><td>{props.year}</td><td>{props.club}</td><td>{props.games}</td><td>{props.points}</td>{buttonDelete}{buttonEdit}</tr>;
    }
        
    return (
        tableRow
    );
        
};
        
export default React.memo(tableRow);
        
       
