﻿using KosarkasiService.Interfaces;
using KosarkasiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace KosarkasiService.Controllers
{
    //[EnableCors("*", "*", "*")]
    public class KluboviController : ApiController
    {
        IKlubRepository _repository { get; set; }

        public KluboviController(IKlubRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Klub> GetAll()
        {
            var klubovi = _repository.GetAll();
            return klubovi;
        }

        [ResponseType(typeof(Klub))]
        public IHttpActionResult GetById(int id)
        {
            var klub = _repository.GetById(id);
            if (klub == null)
            {
                return NotFound();
            }
            return Ok(klub);
        }

        [Route("api/ekstremi")]
        public IEnumerable<Klub> GetBestAndWorst()
        {
            var klubBest = _repository.GetAll().OrderByDescending(k => k.Trofeji).Take(1).FirstOrDefault();
            var klubWorst = _repository.GetAll().OrderBy(k => k.Trofeji).Take(1).FirstOrDefault();

            List <Klub> ekstremi = new List<Klub>();
            ekstremi.Add(klubBest);
            ekstremi.Add(klubWorst);

            return ekstremi.OrderBy(k=>k.Trofeji);
               
        }

    }
}
