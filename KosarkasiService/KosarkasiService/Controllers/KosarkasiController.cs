﻿using AutoMapper;
using KosarkasiService.Interfaces;
using KosarkasiService.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;


namespace KosarkasiService.Controllers
{
    //[EnableCors("http://localhost:3000", "*", "*")]
    public class KosarkasiController : ApiController
    {
        IKosarkasRepository _repository { get; set; }

        public KosarkasiController(IKosarkasRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<KosarkasDTO> GetAll()
        {
            var kosarkasi = _repository.GetAll().OrderByDescending(k=>k.PoeniProsek).ToList();

            List<KosarkasDTO> kosarkasiDTO = Mapper.Map<List<KosarkasDTO>>(kosarkasi);

            return kosarkasiDTO;
        }

        [Authorize]
        [ResponseType(typeof(KosarkasDTO))]
        public IHttpActionResult GetById(int id)
        {
            var kosarkas = _repository.GetById(id);
            if (kosarkas == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<KosarkasDTO>(kosarkas));
        }

        public IEnumerable<KosarkasDTO> GetByYear(int godine)
        {
            var kosarkasi = _repository.GetAll().Where(k=>k.GodinaRodjenja > godine).OrderBy(k=>k.GodinaRodjenja);
            List<KosarkasDTO> kosarkasiDTO = Mapper.Map<List<KosarkasDTO>>(kosarkasi);

            return kosarkasiDTO;
        }

        [ResponseType(typeof(KosarkasDTO))]
        public IHttpActionResult Post(Kosarkas kosarkas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(kosarkas);
            return CreatedAtRoute("DefaultApi", new { id = kosarkas.Id }, Mapper.Map<KosarkasDTO>(kosarkas));
        }

        [Authorize]
        [ResponseType(typeof(KosarkasDTO))]
        public IHttpActionResult Put(int id, Kosarkas kosarkas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (kosarkas.Id != id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(kosarkas);

            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return Ok(Mapper.Map<KosarkasDTO>(kosarkas));
        }

        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(int id)
        {
            var kosarkas = _repository.GetById(id);
            if (kosarkas == null)
            {
                return NotFound();
            }

            _repository.Delete(kosarkas);
            return StatusCode(HttpStatusCode.NoContent);
        }

        [Authorize]
        [Route("api/pretraga")]
        public IEnumerable<KosarkasDTO> PostFromToGames(KosarkasFilter filter)
        {
            var kosarkasi = _repository.GetAll().Where(k => k.BrUtakmica >= filter.Najmanje && k.BrUtakmica <= filter.Najvise).OrderByDescending(k => k.PoeniProsek).ToList();
            return Mapper.Map<List<KosarkasDTO>>(kosarkasi);
        }


    }
}
