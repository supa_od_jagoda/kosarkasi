﻿using KosarkasiService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KosarkasiService.Models;

namespace KosarkasiService.Repositories
{
    public class KlubRepository : IDisposable, IKlubRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Klub> GetAll()
        {
            return db.Klubovi;
        }

        public Klub GetById(int id)
        {
            return db.Klubovi.FirstOrDefault(k => k.Id == id);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}