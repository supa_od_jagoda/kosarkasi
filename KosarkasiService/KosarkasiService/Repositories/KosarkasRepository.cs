﻿using KosarkasiService.Interfaces;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KosarkasiService.Models;
using System.Data.Entity.Infrastructure;

namespace KosarkasiService.Repositories
{
    public class KosarkasRepository : IDisposable, IKosarkasRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Kosarkas kosarkas)
        {
            db.Kosarkasi.Add(kosarkas);
            db.SaveChanges();
        }

        public void Delete(Kosarkas kosarkas)
        {
            db.Kosarkasi.Remove(kosarkas);
            db.SaveChanges();
        }

        public IEnumerable<Kosarkas> GetAll()
        {
            return db.Kosarkasi.Include(k => k.Klub);
        }

        public Kosarkas GetById(int id)
        {
            return db.Kosarkasi.Include(k => k.Klub).FirstOrDefault(k => k.Id == id);
        }

        public void Update(Kosarkas kosarkas)
        {
            db.Entry(kosarkas).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}