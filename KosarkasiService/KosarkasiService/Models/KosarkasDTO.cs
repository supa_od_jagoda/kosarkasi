﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KosarkasiService.Models
{
    public class KosarkasDTO
    {
        public int Id { get; set; }
        public string ImePrezime { get; set; }
        public int GodinaRodjenja { get; set; }
        public int BrUtakmica { get; set; }
        public double PoeniProsek { get; set; }

        public int KlubId { get; set; }
        public string KlubNaziv { get; set; }
    }
}