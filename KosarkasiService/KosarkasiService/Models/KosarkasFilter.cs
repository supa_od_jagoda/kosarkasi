﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KosarkasiService.Models
{
    public class KosarkasFilter
    {
        public int Najmanje { get; set; }
        public int Najvise { get; set; }
    }
}