﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KosarkasiService.Models
{
    public class Klub
    {
        public int Id { get; set; }
        [Required]
        [StringLength(maximumLength:50, MinimumLength =1)]
        public string Naziv { get; set; }
        [StringLength(maximumLength:3, MinimumLength =3)]
        public string Liga { get; set; }
        [Required]
        [Range(minimum:1945, maximum:1999)]
        public int GodinaOsnivanja { get; set; }
        [Required]
        [Range(minimum:0, maximum:19)]
        public int Trofeji { get; set; }
    }
}