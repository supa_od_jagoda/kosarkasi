﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KosarkasiService.Models
{
    public class Kosarkas
    {
        public int Id { get; set; }
        [Required]
        [StringLength(maximumLength:40, MinimumLength =1)]
        public string ImePrezime { get; set; }
        [Required]
        [Range(maximum:1999, minimum:1976)]
        public int GodinaRodjenja { get; set; }
        [Required]
        [Range(minimum:1, maximum:int.MaxValue)]
        public int BrUtakmica { get; set; }
        [Required]
        [Range(minimum:0.1, maximum:29)]
        public double PoeniProsek { get; set; }

        public int KlubId { get; set; }
        public Klub Klub { get; set; }
    }
}