﻿$(document).ready(function () {
	// podaci od interesa
	var host = window.location.host;
	var token = null;
	var headers = {};
	//var formAction = "Create";
	var editingId;
	showKosarkase();


	// pripremanje dogadjaja za brisanje i izmenu - mora ovako jer dugmad ne postoji u trenutku poziva document.ready
	$("body").on("click", "#btnDeleteKosarkas", deleteKosarkas);
	$("body").on("click", "#btnEditKosarkas", editKosarkas);


	//dugme Registracija i Prijava
	$("#priIReg").click(function () {

		showBlock("#priDiv");
		showBlock("#priRegHeader");
		hideBlock("#pocetniHeader");

	});

	//dugme Pocetak
	$("#pocetak").click(function () {

		showBlock("#pocetniHeader");
		hideBlock("#priRegHeader");
		hideBlock("#priDiv");
		$("#priIme").val("");//praznimo formu za prijavu
		$("#priLoz").val("");
		
	});


	// registracija korisnika
	$("#registracija").submit(function (e) {
		e.preventDefault();

		var korIme = $("#priIme").val();
		var loz1 = $("#priLoz").val();
		var loz2 = $("#priLoz").val();

		// objekat koji se salje
		var sendData = {
			"Email": korIme,
			"Password": loz1,
			"ConfirmPassword": loz2
		};

		$.ajax({
			type: "POST",
			url: 'http://' + host + "/api/Account/Register",
			data: sendData

		}).done(function (data) {
			$("#info").append("Uspešna registracija na sistem!");//prikazujemo poruku da je registracija uspela u info div
			$("#priIme").val("");//praznimo formu za prijavu
			$("#priLoz").val("");

		}).fail(function (data) {
			alert("Greska prilikom prijave");
		});

	});

	// prijava korisnika
	$("#btnPrijava").click(function (e) {
		e.preventDefault();

		var korIme = $("#priIme").val();
		var loz = $("#priLoz").val();

		// objekat koji se salje
		var sendData = {
			"grant_type": "password",
			"username": korIme,
			"password": loz
		};

		$.ajax({
			"type": "POST",
			"url": 'http://' + host + "/Token",
			"data": sendData

		}).done(function (data) {
			console.log(data);
			$("#priInfo").html(data.userName);//ispisujemo korisnicke podatke
			$("#info").empty();//praznimo string iz info diva o uspesnoj registraciji
			token = data.access_token;
			hideBlock("#priDiv");//sakrivamo formu za prijavu
			showBlock("#header");//pokazujemo podatke o korisniku i input za pretragu
			hideBlock("#priRegHeader");
			$("#priIme").val("");//praznimo formu za prijavu
			$("#priLoz").val("");

			// Prikazi sve kosarkase nakon sto se prijava izvrsi.
			showKosarkase();

		}).fail(function (data) {
			alert("Greska prilikom prijave");
		});
	});


	// odjava korisnika sa sistema
	$("#odjavise").click(function () {
		token = null;
		headers = {};

		$("#priIme").val("");//praznimo formu za prijavu
		$("#priLoz").val("");
		showBlock("#pocetniHeader");
		hideBlock("#header");
		$("#info").empty();//praznimo info(to ni ne mora sada mozda)
		$("#priInfo").empty();//praznimo informacije o prijavljenom korisniku
		refreshKosarkasiTable();//ponovo ucitamo tabelu da bismo obezbedili prikaz neprijavljenom korisniku
		hideBlock("#formKosarkas");//sakriva se i forma za izmenu za slucaj da je otvorena u trenutku odjave

	});

	// ODUSTANI OD IZMENE kosarkasa
	$("#odustani").click(function () {

		hideBlock("#formKosarkas");
		$("#Name").val('');
		$("#BirthYear").val('');
		$("#GamesNumber").val('');
		$("#KlubId").val('');
		$("#PointsAverage").val('');

	});

	// ucitavanje tabele sa kosarkasima
	function setKosarkase(data) {
		console.log("Status: success");

		var $container = $("#data");
		$container.empty();

		console.log(data);
		// ispis naslova
		var div = $("<div></div>");
		var h1 = $("<h1>Kosarkasi</h1>");
		div.append(h1);
		// ispis tabele
		var table = $("<table class='table table-bordered table-hover'></table>");
		var tbody = $("<tbody></tbody>");
		var header = $("<tr style='background-color:yellow'><th>Ime i Prezime</th><th>Rodjenje</th><th>Klub</th><th>Utakmice</th><th>Poeni</th>");
		//dodaju se jos dve kolone u header tabele ukoliko postoji token
		if (token) {
			header.append("<th>Brisanje</th><th>Izmena</th></tr>");
		} else {
			header.append("</tr>");
		}
		tbody.append(header);
		//ide se kroz svaki clan liste i pravi se novi red
		for (i = 0; i < data.length; i++) {
			// prikazujemo novi red u tabeli
			var row = "<tr>";
			// prikaz podataka
			var displayData = "<td>" + data[i].ImePrezime + "</td><td>" + data[i].GodinaRodjenja + "</td><td>" + data[i].KlubNaziv + "</td><td>" + data[i].BrUtakmica + "</td><td>" + data[i].PoeniProsek + "</td>";
			// prikaz dugmadi za izmenu i brisanje
			var stringId = data[i].Id.toString();
			//ukoliko postoji token dodaju se jos dve kolone u red
			if (token) {

				var displayDelete = "<td><button class='btn btn-default btn-md' id=btnDeleteKosarkas name=" + stringId + ">Obrisi</button></td>";
				var displayEdit = "<td><button class ='btn btn-default btn-md' id=btnEditKosarkas name=" + stringId + ">Izmeni</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";

			} else {
				row += displayData + "</tr>";
			}
			tbody.append(row);
			//newId = data[i].Id;
		}
		table.append(tbody);
		div.append(table);
		div.append();

		//// ispis novog sadrzaja
		$container.append(div);

	}

	//Ajax poziv za GET
	function showKosarkase() {

		$.ajax({
			"type": "GET",
			"url": "http://" + host + "/api/kosarkasi",

		}).done(function (data) {
			setKosarkase(data);
		}).fail(function (data) {
			alert(data);
		});
	}

	//Ajax poziv za PUT unutar submita
	$("#kosarkasForm").submit(function (e) {
		// sprecavanje default akcije forme
		e.preventDefault();

		var name = $("#Name").val();
		var birthYear = $("#BirthYear").val();
		var gamesNumber = $("#GamesNumber").val();
		var klub = $("#KlubId").val();
		var pointsAverage = $("#PointsAverage").val();

		var ok = true;
		if (!name) {
			alert("Ime je obavezno polje.");
			ok = false;
		} else if (name.length < 1 || name.length > 40) {
			alert("Ime mora imati izmedju 1 i 40 karaktera.");
			ok = false;
		} else if (!birthYear) {
			alert("Godina rodjenja je obavezno polje.");
			ok = false;
		} else if (birthYear < 1976 || birthYear > 1999) {
			alert("Godina rodjenja mora biti izmedju 1976 i 1999.");
			ok = false;
		} else if (gamesNumber < 0) {
			alert("Broj utakmica za reprezentaciju mora biti veci od 0");
			ok = false;
		} else if (pointsAverage < 0.1 || pointsAverage > 29) {
			alert("Prosek poena mora biti veci od 0 i manji od 30");
			ok = false;
		}
		if (!ok) {
			return;
		}


		if (token) {
			headers.Authorization = 'Bearer ' + token;
		}


		var httpAction = "PUT";
		var url = "http://" + host + "/api/kosarkasi/" + editingId.toString();
		var sendData = {
			"Id": editingId,
			"ImePrezime": name,
			"GodinaRodjenja": birthYear,
			"BrUtakmica": gamesNumber,
			"PoeniProsek": pointsAverage,
			"KlubId": klub,
		}

		console.log("Objekat za slanje");
		console.log(sendData);

		$.ajax({
			"type": httpAction,
			"url": url,
			"headers": headers,
			"data": sendData
		})
			.done(function (data) {
				refreshKosarkasiTable();//refreshovanje tabele
				hideBlock("#formKosarkas");//nakon edita sakriva se forma
			})
			.fail(function (data) {
				alert("Greska prilikom izmene!");
			})

	});

	//AJAX poziv za brisanje kosarkasa
	function deleteKosarkas() {
		// izvlacimo {id}
		if (token) {
			headers.Authorization = 'Bearer ' + token;
		}

		//Odnosi se na name html elementa koji je pozvao funkciju delete (ovu)
		var deleteId = this.name;
		// saljemo zahtev 
		$.ajax({
			"url": "http://" + host + "/api/kosarkasi/" + deleteId.toString(),
			"type": "DELETE",
			"headers": headers,

		})
			.done(function (data) {
				refreshKosarkasiTable();//ponovno ucitavanje tabele
			})
			.fail(function (data) {
				alert("Greska prilikom brisanja!");
			});

	};

	//dobavljanje kosarkasa sa GetById
	function editKosarkas() {
		// izvlacimo id
		if (token) {
			headers.Authorization = 'Bearer ' + token;
		}

		//Postavljanje klubova u drop down liste
		setKluboviDropdown("#KlubId");

		showBlock("#formKosarkas");//prikazati formu za edit kosarkasa

		//Odnosi se na name html elementa koji je pozvao funkciju edit
		var editId = this.name;
		// saljemo zahtev da dobavimo tog kosarkasa
		$.ajax({
			"url": "http://" + host + "/api/kosarkasi/" + editId.toString(),
			"type": "GET",
			"headers": headers,
		})
			.done(function (data) {//punimo formu postojecim podacima
				$("#Name").val(data.ImePrezime);
				$("#BirthYear").val(data.GodinaRodjenja);
				$("#GamesNumber").val(data.BrUtakmica);
				$("#KlubId").val(data.KlubId);
				$("#PointsAverage").val(data.PoeniProsek);
				editingId = data.Id;
			})
			.fail(function (data) {
				alert("Greska prilikom izmene!");
			});

	};

	//ponovno ucistavanje tabele sa podacima
	function refreshKosarkasiTable() {
		// cistim formu
		$("#Name").val('');
		$("#BirthYear").val('');
		$("#GamesNumber").val('');
		$("#KlubId").val('');
		$("#PointsAverage").val('');

		showKosarkase();
	};

	//funkcija za GET klubova i smestanje u Dropdown listu
	function setKluboviDropdown(selectId) {
		$(selectId).empty();
		var requestUrl = 'http://' + host + "/api/klubovi";
		$.getJSON(requestUrl, function (data, status) {
			$.each(data, function (i) {
				$(selectId).append($("<option></option>").val(data[i].Id).html(data[i].Naziv));
			});
		});
	}

	//PRETRAGA po filteru
	$("#btnFilter").click(function () {
		if (token) {
			headers.Authorization = 'Bearer ' + token;
		}

		var najmanje = $("#najmanje").val();
		var najvise = $("#najvise").val();


		if (!najmanje || !najvise) {
			alert("Pogresni filter za pretragu");
			return;
		}

		//if (najvise > najmanje) { ne razumem zbog cega ovo zapravo radi dobru validaciju - nisam uspela da otkrijem problem
		//	alert("Pogresni filter za pretragu");
		//	return;
		//}

		// saljemo zahtev na api/pretraga
		$.ajax({
			"url": "http://" + host + "/api/pretraga",
			"type": "POST",
			"headers": headers,
			"data": { "Najmanje": najmanje, "Najvise": najvise }
		})
			.done(function (data) {
				setKosarkase(data);
			})
			.fail(function (data) {
				alert("Desila se greska prilikom pretrage!");
			});
	});

	//funkcije za otkrivanje i sakrivanje elemenata
	function showBlock(elementId) {
		$(elementId).css("display", "block");
	}

	function hideBlock(elementId) {
		$(elementId).css("display", "none");
	}



});