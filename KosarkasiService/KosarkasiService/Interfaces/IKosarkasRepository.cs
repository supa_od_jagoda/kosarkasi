﻿using KosarkasiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KosarkasiService.Interfaces
{
    public interface IKosarkasRepository
    {
        IEnumerable<Kosarkas> GetAll();
        Kosarkas GetById(int id);
        void Add(Kosarkas kosarkas);
        void Update(Kosarkas kosarkas);
        void Delete(Kosarkas kosarkas);
    }
}
