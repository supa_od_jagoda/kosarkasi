﻿using KosarkasiService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KosarkasiService.Interfaces
{
    public interface IKlubRepository
    {
        IEnumerable<Klub> GetAll();
        Klub GetById(int id);
    }
}
