namespace KosarkasiService.Migrations
{
    using KosarkasiService.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KosarkasiService.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(KosarkasiService.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Klubovi.AddOrUpdate(
                k=>k.Id,
                new Klub { Id=1, Naziv= "Sacramento Kings", Liga = "NBA", GodinaOsnivanja = 1985, Trofeji = 5 },
                new Klub { Id=2, Naziv= "Dallas Mavericks", Liga = "NBA", GodinaOsnivanja = 1980, Trofeji = 6 },
                new Klub { Id=3, Naziv= "Indiana Pacers", Liga = "NBA", GodinaOsnivanja = 1967, Trofeji = 13 }
                );

            context.Kosarkasi.AddOrUpdate(
                ks=>ks.Id,
                new Kosarkas { Id=1, ImePrezime="Bogdan Bogdanovic", GodinaRodjenja = 1992, BrUtakmica = 96 , PoeniProsek = 12.3 , KlubId = 1 },
                new Kosarkas { Id=2, ImePrezime="Luka Doncic", GodinaRodjenja = 1999, BrUtakmica = 26 , PoeniProsek = 18.2 , KlubId = 2 },
                new Kosarkas { Id=3, ImePrezime="Bojan Bogdanovic", GodinaRodjenja = 1989, BrUtakmica = 105 , PoeniProsek = 14.8, KlubId = 3 },
                new Kosarkas { Id=4, ImePrezime="Nemanja Bjelica", GodinaRodjenja = 1988, BrUtakmica = 25 , PoeniProsek = 10.8, KlubId = 1 }

                );

        }
    }
}
