﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using KosarkasiService.Repositories;
using KosarkasiService.Interfaces;
using Unity;
using Unity.Lifetime;
using KosarkasiService.Resolvers;
using AutoMapper;
using KosarkasiService.Models;
using System.Web.Http.Cors;

namespace KosarkasiService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Cors
            //var cors = new EnableCorsAttribute("*", "*", "GET, POST, OPTIONS");
            //config.EnableCors(cors);

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            Mapper.Initialize(cfg => {
                cfg.CreateMap<Kosarkas, KosarkasDTO>().ForMember(dto => dto.KlubNaziv, opt => opt.MapFrom(src => src.Klub.Naziv)); // automatski će mapirati Author.Name u AuthorName
                //.ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src => src.Author.Name)); // ako želimo eksplicitno zadati mapranje
            });


            var container = new UnityContainer();
            container.RegisterType<IKlubRepository, KlubRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IKosarkasRepository, KosarkasRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);                        
        }
    }
}
