﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using KosarkasiService.Models;
using KosarkasiService.Interfaces;
using Moq;
using KosarkasiService.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;

namespace KosarkasiService.Tests.Controllers
{
    [TestClass]
    public class KosarkasiControllerTest
    {
        private static void InitializeMapper()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Kosarkas, KosarkasDTO>().ForMember(dto => dto.KlubNaziv, opt => opt.MapFrom(src => src.Klub.Naziv));
            });
        }

        [TestMethod]
        public void GetReturnsKosarkasWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IKosarkasRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Kosarkas { Id = 42 });

            InitializeMapper();

            var controller = new KosarkasiController(mockRepository.Object);

            // Act 
            IHttpActionResult actionResult = controller.GetById(42);

            var contentResult = actionResult as OkNegotiatedContentResult<KosarkasDTO>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IKosarkasRepository>();
            var controller = new KosarkasiController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetById(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IKosarkasRepository>();
            var controller = new KosarkasiController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Kosarkas { Id = 9, ImePrezime = "Kosarkas2", BrUtakmica = 105, GodinaRodjenja = 1996, KlubId = 3, PoeniProsek = 12.1 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void PostReturnsMultipleObjects()
        {
            // Arrange
            List<Kosarkas> kosarkasi = new List<Kosarkas>();
            kosarkasi.Add(new Kosarkas { Id = 1, ImePrezime = "Kosarkas1", BrUtakmica = 75, PoeniProsek = 7 });
            kosarkasi.Add(new Kosarkas { Id = 2, ImePrezime = "Kosarkas2", BrUtakmica = 25, PoeniProsek = 11.1});
            kosarkasi.Add(new Kosarkas { Id = 3, ImePrezime = "Kosarkas3", BrUtakmica = 100, PoeniProsek= 12.1 });


            InitializeMapper();


            var mockRepository = new Mock<IKosarkasRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(kosarkasi.AsEnumerable());
            var controller = new KosarkasiController(mockRepository.Object);
            var filter = new KosarkasFilter()
            {
                Najmanje = 74,
                Najvise = 110
            };


            // Act
            IEnumerable<KosarkasDTO> result = controller.PostFromToGames(filter);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.ToList().Count);
            Assert.AreEqual(kosarkasi.ElementAt(2).Id, result.ElementAt(0).Id);
            Assert.AreEqual(kosarkasi.ElementAt(0).Id, result.ElementAt(1).Id);
        }


    }
}
